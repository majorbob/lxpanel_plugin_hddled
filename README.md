# HDDLED
A plugin for LXPanel to show hard drive activity.

Installation
============
Copy hddled.so to the plugins directory:
 Ubuntu 32-bit = /usr/lib/i386-linux-gnu/lxpanel/plugins/
 Ubuntu 64-bit = /usr/lib/x64..../lxpanel/plugins/
 Others = /usr/lib/lxpanel/plugins/ 
and images to /usr/share/lxpanel/images
then use LXPanel to add the HDD LED plugin.

Limitations
===========
Currently only one instance can be added to a panel, although the drive monitored 
can be selected.

Compiling from Source
===================
Compile for your system as per the LXPanel plugins instructions:
gcc -Wall `pkg-config --cflags gtk+-2.0 lxpanel` -shared -fPIC hddled.c -o hddled.so `pkg-config --libs lxpanel`


Author
======
Benny Roberts

License
=========
This code is based on the kbled.c plugin from the LXDE developers, and as such is 
licensed:

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

The GPL2 can be downloaded from here:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
